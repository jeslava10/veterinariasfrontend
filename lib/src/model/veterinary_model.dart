import 'package:veterinary/src/model/users_model.dart';

class Veterinary {
  int id;
  String name;
  String nit;
  String email;
  Users users;
  bool state;

  Veterinary(
      {this.id, this.name, this.nit, this.email, this.users, this.state});

  factory Veterinary.fromJson(Map<String, dynamic> parsedJson) {
    return Veterinary(
        id: parsedJson['id'],
        name: parsedJson['name'],
        nit: parsedJson['nit'],
        email: parsedJson['email'],
        users: Users.fromJson(parsedJson['users']),
        state: parsedJson['state']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'nit': nit,
        'email': email,
        'users': users.toJson(),
        'state': state
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'nit': nit,
        'email': email,
        'users': users.toJsonRegistry(),
        'state': state
      };
}
