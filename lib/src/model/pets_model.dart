import 'package:veterinary/src/model/users_model.dart';

class Pets {
  int id;
  String namePet;
  int agePet;
  String breedPet;
  String observationsPet;
  String sexPet;
  String speciesPet;
  Users users;
  bool statusPet;

  Pets(
      {this.id,
      this.namePet,
      this.agePet,
      this.breedPet,
      this.observationsPet,
      this.sexPet,
      this.speciesPet,
      this.users,
      this.statusPet});

  factory Pets.fromJson(Map<String, dynamic> parsedJson) {
    return Pets(
        id: parsedJson['id'],
        namePet: parsedJson['namePet'],
        agePet: parsedJson['agePet'],
        breedPet: parsedJson['breedPet'],
        observationsPet: parsedJson['observationsPet'],
        sexPet: parsedJson['sexPet'],
        speciesPet: parsedJson['speciesPet'],
        users: Users.fromJson(parsedJson['users']),
        statusPet: parsedJson['statusPet']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'namePet': namePet,
        'agePet': agePet,
        'breedPet': breedPet,
        'observationsPet': observationsPet,
        'sexPet': sexPet,
        'speciesPet': speciesPet,
        'users': users.toJson(),
        'statusPet': statusPet
      };

  Map<String, dynamic> toJsonRegistry() => {
        'namePet': namePet,
        'agePet': agePet,
        'breedPet': breedPet,
        'observationsPet': observationsPet,
        'sexPet': sexPet,
        'speciesPet': speciesPet,
        'users': users.toJsonRegistry(),
        'statusPet': statusPet
      };
}
