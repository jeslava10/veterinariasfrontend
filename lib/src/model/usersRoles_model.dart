class UsersRoles {
  int id;
  String description;

  UsersRoles({
    this.id,
    this.description
  });

  factory UsersRoles.fromJson(Map<String, dynamic> parsedJson) {
    return UsersRoles(
      id: parsedJson['id'],
      description: parsedJson['description']
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'description': description
  };

  Map<String, dynamic> toJsonRegistry() => {
    'description': description
  };

}