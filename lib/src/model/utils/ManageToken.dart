
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert' show json, base64, ascii;

const SERVER_IP = 'http://192.168.1.167:5000';
final storage = FlutterSecureStorage();

class ManageToken{

  Future<String> get jwtOrEmpty async {
    var jwt = await storage.read(key: "jwt");
    if(jwt == null) return "";
    return jwt;
  }

  Future<String> attemptLogIn(String username, String password) async {
  var res = await http.post(
    "$SERVER_IP/login",
    body: {
      "username": username,
      "password": password
    }
  );
  if(res.statusCode == 200) return res.body;
  return null;
}

Future<int> attemptSignUp(String username, String password) async {
  var res = await http.post(
    '$SERVER_IP/signup',
    body: {
      "username": username,
      "password": password
    }
  );
  return res.statusCode;  
}


}

