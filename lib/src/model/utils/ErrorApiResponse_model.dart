class ErrorApiResponse {

  int statusResponse;
  String message;
  Object object;

  ErrorApiResponse({this.statusResponse, this.object, this.message});

  factory ErrorApiResponse.fromJson(Map<String, dynamic> json) {
    return ErrorApiResponse(
        statusResponse: json['statusResponse'], 
        message: json['message'], 
        object: json['object']);
  }

  Map<String, dynamic> toJson() => {'statusResponse': statusResponse
    'message': message,
    'object': object};
}
