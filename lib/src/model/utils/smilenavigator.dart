import 'package:flutter/material.dart';
import 'package:veterinary/src/resource/constant.dart';

class SmileNavigator{
  static void goToIntro(BuildContext context){
    Navigator.of(context).pushNamedAndRemoveUntil(Constant.introRoute, (Route<dynamic> route) => false);
  }
}