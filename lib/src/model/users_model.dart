import 'package:veterinary/src/model/documentTypes_model.dart';
import 'package:veterinary/src/model/usersRoles_model.dart';

class Users {
  int id;
  String name;
  String lastName;
  String neighborhoodResidence;
  String cityResidence;
  String departmentResidence;
  String documentNumber;
  DocumentTypes documentTypes;
  UsersRoles usersRoles;
  DateTime birthdate;
  String email;
  String password;
  String phone;
  bool state;

  Users(
      {this.id,
      this.name,
      this.lastName,
      this.neighborhoodResidence,
      this.cityResidence,
      this.departmentResidence,
      this.documentNumber,
      this.documentTypes,
      this.usersRoles,
      this.birthdate,
      this.email,
      this.password,
      this.phone,
      this.state});

  factory Users.fromJson(Map<String, dynamic> parsedJson) {
    return Users(
        id: parsedJson['id'],
        name: parsedJson['name'],
        lastName: parsedJson['lastName'],
        neighborhoodResidence: parsedJson['neighborhoodResidence'],
        cityResidence: parsedJson['cityResidence'],
        departmentResidence: parsedJson['departmentResidence'],
        documentNumber: parsedJson['documentNumber'],
        documentTypes: DocumentTypes.fromJson(parsedJson['documentTypes']),
        usersRoles: UsersRoles.fromJson(parsedJson['userRoles']),
        birthdate: parsedJson['birthdate'],
        email: parsedJson['email'],
        password: parsedJson['password'],
        phone: parsedJson['phone'],
        state: parsedJson['state']);
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'lastName': lastName,
        'neighborhoodResidence': neighborhoodResidence,
        'cityResidence': cityResidence,
        'departmentResidence': departmentResidence,
        'documentNumber': documentNumber,
        'documentTypes': documentTypes.toJson(),
        'usersRoles': usersRoles.toJson(),
        'birthdate': birthdate,
        'email': email,
        'password': password,
        'phone': phone,
        'state': state
      };

  Map<String, dynamic> toJsonRegistry() => {
        'name': name,
        'lastName': lastName,
        'neighborhoodResidence': neighborhoodResidence,
        'cityResidence': cityResidence,
        'departmentResidence': departmentResidence,
        'documentNumber': documentNumber,
        'documentTypes': documentTypes.toJsonRegistry(),
        'usersRoles': usersRoles.toJsonRegistry(),
        'birthdate': birthdate,
        'email': email,
        'password': password,
        'phone': phone,
        'state': state
      };
}
