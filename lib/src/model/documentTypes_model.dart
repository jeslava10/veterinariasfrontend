class DocumentTypes {
  int id;
  String description;

  DocumentTypes({
    this.id,
    this.description
  });

  factory DocumentTypes.fromJson(Map<String, dynamic> parsedJson) {
    return DocumentTypes(
      id: parsedJson['id'],
      description: parsedJson['description']
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'description': description
  };

  Map<String, dynamic> toJsonRegistry() => {
    'description': description
  };
}