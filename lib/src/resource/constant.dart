import 'package:flutter/cupertino.dart';

class Constant {
  static const String urlVeterinary = "http://13.59.238.237:8889";

  static const String pathBase = "/api/veterinaria";
  static const String contentTypeHeader = "application/json";
  static const String authorizationHeader = "Bearer";

  //Api Document Types
  static const String urlFindDocumentTypes = pathBase + "/documentTypes/all";
  static const String urlSaveDocumentTypes = pathBase + "/documentTypes/save";
  static const String urlUpdateDocumentTypes =
      pathBase + "/documentTypes/update";

  //Api Users Roles
  static const String urlFindUsersRoles = pathBase + "/usersRoles/all";
  static const String urlSaveUsersRoles = pathBase + "/usersRoles/save";
  static const String urlUpdateUsersRoles = pathBase + "/usersRoles/update";

  //Api Users
  static const String urlFindUsers = pathBase + "/users/all";
  static const String urlSaveUsers = pathBase + "/users/save";
  static const String urlUpdateUsers = pathBase + "/users/update";

  //Api Pets
  static const String urlFindPets = pathBase + "/pets/all/name";
  static const String urlSavePets = pathBase + "/pets/save";
  static const String urlUpdatePets = pathBase + "/pets/update";

  //Api Veterinary
  static const String urlFindVeterinary = pathBase + "/veterinary/all";
  static const String urlSaveVeterinary = pathBase + "/veterinary/save";
  static const String urlUpdateVeterinary = pathBase + "/veterinary/update";

  //Message
  static const String createMessage = "OK";
  static const String savingObj = "Objeto guardado";
  

  //Routes
  static Route<Object> introRoute; 
  
}
