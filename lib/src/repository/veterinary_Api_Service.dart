import 'dart:convert';
import 'dart:io';
import 'package:veterinary/src/model/Veterinary_model.dart';
import 'package:veterinary/src/model/utils/apiResponse_model.dart';
import 'package:veterinary/src/resource/constant.dart';
import 'package:f_logs/f_logs.dart';
import 'package:veterinary/src/model/utils/ErrorApiResponse_model.dart';
import 'package:http/http.dart' as http;

class VeterinaryApiService {
  Veterinary _veterinary;
  ErrorApiResponse _error;
  VeterinaryApiService();

  Future<ApiResponse> getAllVeterinary(String accessToken) async {
    List<Veterinary> listVeterinary = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constant.urlVeterinary, Constant.urlFindVeterinary);
    var res = await http.get(url, headers: {HttpHeaders.authorizationHeader: "Bearer" + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listVeterinary = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listVeterinary.add(Veterinary.fromJson(i));
        return i;
      });
      apiResponse.object = listVeterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertVeterinary(
      Veterinary veterinary, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(veterinary.toJsonRegistry());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlSaveVeterinary);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer" + accessToken
        },
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _veterinary = Veterinary.fromJson(resBody);
      apiResponse.object = _veterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateVeterinary(
      Veterinary veterinary, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(veterinary.toJson());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlUpdateVeterinary);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer" + accessToken
        },
        body: body);
    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _veterinary = Veterinary.fromJson(resBody);
      apiResponse.object = _veterinary;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }
}
