import 'dart:convert';
import 'dart:io';
import 'package:veterinary/src/model/Pets_model.dart';
import 'package:veterinary/src/model/utils/apiresponse_model.dart';
import 'package:veterinary/src/model/utils/ErrorApiResponse_model.dart';
import 'package:veterinary/src/resource/constant.dart';
import 'package:f_logs/f_logs.dart';
import 'package:http/http.dart' as http;

class PetsApiService {
  Pets _pets;
  ErrorApiResponse _error;
  PetsApiService();

  Future<ApiResponse> getAllNamePets(String accessToken) async {
    List<Pets> listPets = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constant.urlVeterinary, Constant.urlFindPets);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer" + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listPets = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listPets.add(Pets.fromJson(i));
        return i;
      });
      apiResponse.object = listPets;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertPets(Pets pets, String accesToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(pets.toJsonRegistry());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlSavePets);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "aplicattion/json",
          HttpHeaders.authorizationHeader: "Bearer " + accesToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _pets = Pets.fromJson(resBody);
      apiResponse.object = _pets;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updatePets(Pets pets, String accesToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(pets.toJson());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlUpdatePets);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "aplicattion/json",
          HttpHeaders.authorizationHeader: "Bearer " + accesToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _pets = Pets.fromJson(resBody);
      apiResponse.object = _pets;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }
}
