import 'package:veterinary/src/model/utils/apiResponse_model.dart';
import 'package:veterinary/src/model/veterinary_model.dart';
import 'package:veterinary/src/model/documentTypes_model.dart';
import 'package:veterinary/src/repository/pets_Api_Service.dart';
import 'package:veterinary/src/repository/usersRoles_Api_Service.dart';
import 'package:veterinary/src/repository/users_Api_Service.dart';
import 'package:veterinary/src/repository/veterinary_Api_Service.dart';
import 'documentTypes_Api_Service.dart';

class GeneralVeterinaryRepository with ManageToken {
  final veterinaryApiService = VeterinaryApiService();
  final petsApiService = PetsApiService();
  final usersApiService = UsersApiService();
  final usersRolesApiService = UsersRolesApiService();
  final documentTypesApiService = DocumentTypesApiService();

//Manage Api Veterinary
  Future<ApiResponse> getAllVeterinary(String accessToken) =>
      veterinaryApiService.getAllVeterinary(accessToken);

  Future<ApiResponse> insertVeterinary(
          Veterinary veterinary, String accessToken) =>
      veterinaryApiService.insertVeterinary(veterinary, accessToken);

  Future<ApiResponse> updateVeterinary(
          Veterinary veterinary, String accessToken) =>
      veterinaryApiService.updateVeterinary(veterinary, accessToken);

//Manage Api DocumentTypes
  Future<ApiResponse> getAllDocumentTypes(String accessToken) =>
      documentTypesApiService.getAllDocumentTypes(accessToken);

  Future<ApiResponse> insertDocumentTypes(
          DocumentTypes documentTypes, String accessToken) =>
      documentTypesApiService.insertDocumentTypes(documentTypes, accessToken);

  Future<ApiResponse> updateDocumentTypes(
          DocumentTypes documentTypes, String accessToken) =>
      documentTypesApiService.updateDocumentTypes(documentTypes, accessToken);

//Manage Api Pets
  Future<ApiResponse> getAllNamePets(String accessToken);

  Future<ApiResponse> insertPets(Pets pets, String accesToken) =>
      petsApiService.insertPets(pets, accesToken);

  Future<ApiResponse> updatePets(Pets pets, String accesToken) =>
      petsApiService.updatePets(pets, accesToken);

//Manage Api UserRoles
  Future<ApiResponse> getUsersRolesAll(String acessToken);

//Manage Api User
  Future<ApiResponse> getAllUser(String accessToken);

  Future<ApiResponse> insertUsers(Users users, String accesToken) =>
      usersApiService.insertUsers(users, accesToken);

  Future<ApiResponse> updateUsers(Users users, String accesToken) =>
      usersApiService.updateUsers(users, accesToken);
}
