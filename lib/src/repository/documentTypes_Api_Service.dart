import 'dart:convert';
import 'dart:io';
import 'package:veterinary/src/model/documentTypes_model.dart';
import 'package:veterinary/src/model/utils/ErrorApiResponse_model.dart';
import 'package:veterinary/src/model/utils/apiResponse_model.dart';
import 'package:veterinary/src/resource/constant.dart';
import 'package:f_logs/f_logs.dart';
import 'package:http/http.dart' as http;

class DocumentTypesApiService {
  DocumentTypes _documentTypes;
  ErrorApiResponse _error;
  DocumentTypesApiService();

  Future<ApiResponse> getAllDocumentTypes(String accessToken) async {
    List<DocumentTypes> listDocumentTypes = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constant.urlVeterinary, Constant.urlFindDocumentTypes);
    var res = await http.get(url,  headers: {HttpHeaders.authorizationHeader: "Bearer" + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listDocumentTypes = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listDocumentTypes.add(DocumentTypes.fromJson(i));
        return i;
      });
      apiResponse.object = listDocumentTypes;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertDocumentTypes(
      DocumentTypes documentTypes, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(documentTypes.toJsonRegistry());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlSaveDocumentTypes);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer" + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _documentTypes = DocumentTypes.fromJson(resBody);

      apiResponse.object = _documentTypes;
    }else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateDocumentTypes(
      DocumentTypes documentTypes, String accessToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(documentTypes.toJsonRegistry());
    Uri uri =
        Uri.http(Constant.urlVeterinary, Constant.urlUpdateDocumentTypes);
    var res = await http.put(uri,
         headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer" + accessToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _documentTypes = DocumentTypes.fromJson(resBody);
      apiResponse.object = _documentTypes;
    }else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }
}
