import 'dart:convert';
import 'dart:io';
import 'package:veterinary/src/model/users_model.dart';
import 'package:veterinary/src/model/utils/apiresponse_model.dart';
import 'package:veterinary/src/resource/constant.dart';
import 'package:f_logs/f_logs.dart';
import 'package:veterinary/src/model/utils/ErrorApiResponse_model.dart';
import 'package:http/http.dart' as http;

class UsersApiService {
  Users _users;
  ErrorApiResponse _error;
  UsersApiService();

  Future<ApiResponse> getAllUser(String accessToken) async {
    List<Users> listUsers = List();
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constant.urlVeterinary, Constant.urlFindPets);
    var res = await http.get(url,
        headers: {HttpHeaders.authorizationHeader: "Bearer" + accessToken});
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    listUsers = List();
    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listUsers.add(Users.fromJson(i));
        return i;
      });
      apiResponse.object = listUsers;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertUsers(Users users, String accesToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(users.toJsonRegistry());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlSaveUsers);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "aplicattion/json",
          HttpHeaders.authorizationHeader: "Bearer " + accesToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _users = Users.fromJson(resBody);
      apiResponse.object = _users;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> updateUsers(Users users, String accesToken) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(users.toJson());
    Uri uri = Uri.http(Constant.urlVeterinary, Constant.urlUpdatePets);
    var res = await http.post(uri,
        headers: {
          HttpHeaders.contentTypeHeader: "aplicattion/json",
          HttpHeaders.authorizationHeader: "Bearer " + accesToken
        },
        body: body);

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _users = Users.fromJson(resBody);
      apiResponse.object = _users;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      FLog.error(text: _error.toJson().toString());
      apiResponse.object = _error;
    }
    return apiResponse;
  }
}
