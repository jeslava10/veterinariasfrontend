import 'package:f_logs/f_logs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:veterinary/src/model/utils/ErrorApiResponse_model.dart';
import 'package:veterinary/src/model/utils/apiResponse_model.dart';
import 'package:veterinary/src/model/Users_model.dart';
import 'package:veterinary/src/repository/generalVeterinary_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:veterinary/src/resource/constant.dart';
import 'package:veterinary/src/model/utils/smilenavigator.dart';
import 'package:f_logs/f_logs.dart';

class UsersBloc {
  BuildContext _context;
  Users _users;
  ProgressDialog _progressDialog;
  String accessToken;
  final _repository = GeneralVeterinaryRepository();
  var _apiResponse = ApiResponse();

  List<Users> _initialList;

  Future<ApiResponse> createUsers(Users users, String accessToken) async {
    _progressDialog = ProgressDialog(_context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    _progressDialog.style(message: Constant.savingObj);
    await _progressDialog.show();
    bool resp = await _repository.validateExpirationSession();
    if (resp) {
      users.date = DataTime.now().toIsoString() + "Z";
      ApiResponse apiResponse =
          await _repository.insertUsers(users, accessToken);
      if (apiResponse.statusResponse == 200) {
        apiResponse.message = Constant.createMessage;
      } else {
        ErrorApiResponse error = apiResponse.object;
        FLog.logThis(
            className: " * ClassName: UsersBloc ",
            methodName: " * MethodName: createUsers ",
            text: " * Message: Status error: " +
                error.status.toString() +
                " Message: " +
                error.message,
            type: LogLevel.ERROR,
            dataLogType: DataLogType.DEVICE.toString());
        await FLog.exportLogs();
      }
      await _progressDialog.hide();
      return apiResponse;
    } else {
      SmileNavigator.goToIntro(_context);
    }
    await _progressDialog.hide();
    return null;
  }

  Future<ApiResponse> updateUsers(Users users, String accessToken) async {
    bool resp = await _repository.validateExpirationSession();
    if (resp) {
      ApiResponse apiResponse =
          await _repository.updateUsers(users, accessToken);
      if (apiResponse.statusResponse == 200) {
        apiResponse.message = Constant.createMessage;
      } else {
        ErrorApiResponse error = apiResponse.object;
        FLog.logThis(
            className: " * ClassName: UsersBloc ",
            methodName: " * MethodName: updateUsers ",
            text: " * Message: Status error: " +
                error.status.toString() +
                " Message: " +
                error.message,
            type: LogLevel.ERROR,
            dataLogType: DataLogType.DEVICE.toString());
        await FLog.exportLogs();
      }
      await _progressDialog.hide();
      return apiResponse;
    } else {
      SmileNavigator.goToIntro(_context);
    }
    await _progressDialog.hide();
    return null;
  }
}
