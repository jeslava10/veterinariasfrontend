import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:veterinary/src/model/documentTypes_model.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:veterinary/src/model/utils/ErrorApiResponse_model.dart';
import 'package:veterinary/src/model/utils/apiResponse_model.dart';
import 'package:veterinary/src/repository/generalVeterinary_repository.dart';
import 'package:veterinary/src/resource/constant.dart';
import 'package:veterinary/src/model/utils/smilenavigator.dart';
import 'package:f_logs/f_logs.dart';


class DocumentTypesBloc{
  BuildContext _context;
  DocumentTypes _documentTypes;
  ProgressDialog _progressDialog;
  String accessToken;

  final _repository = GeneralVeterinaryRepository();
  var _apiResponse = ApiResponse();
  final _documentTypesListController = 
      StreamController<List<DocumentTypes>>.broadcast();
      final _docuentTypesController = StreamController<DocumentTypes>.broadcast();
  
  List<DocumentTypes> _initialList;
  Stream<List<DocumentTypes>> get documentTypesList =>
    _documentTypesListController.stream.asBroadcastStream();
  Stream<DocumentTypes> get documentTypes =>
    _docuentTypesController.stream.asBroadcastStream();
  ApiResponse get apiResponse => _apiResponse;

  DocumentTypesBloc(BuildContext context){
    _context = context;
    _documentTypes = DocumentTypes();
  }

  Future<ApiResponse> insertDocumentTypes(DocumentTypes documentTypes) async {
    _progressDialog = ProgressDialog(_context,
    type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    _progressDialog.style(message: Constant.savingObj);
    await _progressDialog.show();
    bool resp = await _repository.validateExpirationSession();
    if(resp){
      ApiResponse apiResponse = await _repository.insertDocumentTypes(documentTypes, accessToken);
      if(apiResponse.statusResponse == 200){
        apiResponse.message = Constant.createMessage;
      } else {
        ErrorApiResponse error = apiResponse.object;
        FLog.logThis(
          className: " * ClassName: DocumentTypesBloc",
          methodName: "* MethodName: insertDocumentTypes",
          text: " * Message: Status error: "+
          error.status.toString() +
            "Message: "+
            error.message,
            type: LogLevel.ERROR,
            dataLogType: DataLogType.DEVICE.toString());
          await FLog.exportLogs();
      }
      await _progressDialog.hide();
      return apiResponse;
    } else {
      SmileNavigator.goToIntro(_context);
    }
    await _progressDialog.hide();
    return null;
  }

  Future<ApiResponse> updateDocumentTypes(DocumentTypes documentTypes) async {
    bool resp = await _repository.validateExpirationSession();
    if(resp){
      ApiResponse apiResponse = await _repository.insertDocumentTypes(documentTypes, accessToken);
      if(apiResponse.statusResponse == 200){
        apiResponse.message = Constant.createMessage;
      } else {
        ErrorApiResponse error = apiResponse.object;
        FLog.logThis(
          className: " * ClassName: DocumentTypesBloc",
          methodName: "* MethodName: insertDocumentTypes",
          text: " * Message: Status error: "+
          error.status.toString() +
            "Message: "+
            error.message,
            type: LogLevel.ERROR,
            dataLogType: DataLogType.DEVICE.toString());
          await FLog.exportLogs();
      }
      await _progressDialog.hide();
      return apiResponse;
    } else {
      SmileNavigator.goToIntro(_context);
    }
    await _progressDialog.hide();
    return null;
  }

}
